﻿CREATE PROCEDURE spUpdateUser
@idNumber VARCHAR(13),
@studentNumber INT,
@staffId INT,
@name VARCHAR(50),
@surname VARCHAR(50),
@grade INT,
@extension VARCHAR(1),
@role VARCHAR(25),
@username INT,
@password VARCHAR(25)
AS
BEGIN

UPDATE [User]
SET
idNumber = @idNumber,
studentNumber = @studentNumber,
staffId = @staffId,
name = @name,
surname = @surname,
grade = @grade,
extension = @extension,
role = @role,
username = @username,
password = @password

WHERE idNumber = @idNumber
END