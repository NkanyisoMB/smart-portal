﻿CREATE TABLE [User] (
idNumber VARCHAR(13) PRIMARY KEY NOT NULL,
studentNumber INT,
staffId INT,
name VARCHAR(50) NOT NULL,
surname VARCHAR(50) NOT NULL,
grade INT,
extension VARCHAR(1),
role VARCHAR(25) NOT NULL,
username INT NOT NULL,
password VARCHAR(25) NOT NULL
)