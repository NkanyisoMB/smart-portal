﻿CREATE PROCEDURE spGetUser
@idNumber VARCHAR(13)
AS
BEGIN
SELECT idNumber, studentNumber, staffId, name, surname, grade, extension, role
FROM [User]
WHERE idNumber = @idNumber
END