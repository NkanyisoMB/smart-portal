﻿CREATE PROCEDURE [dbo].[spInsertUser]
@idNumber VARCHAR(13),
@studentNumber INT,
@staffId INT,
@name VARCHAR(50),
@surname VARCHAR(50),
@grade INT,
@extension VARCHAR(1),
@role VARCHAR(25),
@username INT,
@password VARCHAR(25)
AS
BEGIN 
INSERT INTO [User] (idNumber, studentNumber, staffId, name, surname, grade, extension, role, username, password)
VALUES (@idNumber, @studentNumber, @staffId, @name, @surname, @grade, @extension, @role, @username, @password);
END