import React, { Component } from 'react';
import fetch from 'isomorphic-fetch';

export default class ViewAccountDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            idNumber: "",
            studentNumber: "0",
            staffId: "",
            name: "",
            surname: "",
            grade: "0",
            extension: "0",
            role: "Admin",
            username: "12345",
            password: ""
        };
    }

    updateField(field, value) {
        const data = this.state;
        data[field] = value;
        this.setState(data);
    }

    submit(e) {
        e.preventDefault();
        const data = this.state;
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append("Access-Control-Allow-Origin", "*");
        headers.append("Content-Type", "application/json");

        fetch('https://localhost:5001/api/register/Admin/', {
            method: 'POST',
            mode: 'no-cors',
            body: JSON.stringify(data),
            headers: headers
        }).then(res => {
            console.log(res)
        }).catch(err => console.log(err + 'Mbense'));

        console.log(`form data: ${JSON.stringify(data)}`);
    }

    render() {
        return (
            <form method="post">
                <div className="container wrapper fadeInDown">
                    <br /><br /><br /><br /><br /><br />
                    <div className="row">
                        <h1 className="col-md-3 offset-md-4">Account Details</h1>
                    </div>
                    <div className="row"><br /></div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="col-md-3 offset-md-4">
                                <div className="form-group">
                                    <label className="form-control">Staff Id</label>
                                    <label className="form-control" id="staffId">{this.state.staffId}</label>
                                </div>
                                <div className="form-group">
                                    <label className="form-control">Id Number</label>
                                    <label className="form-control" id="idNumber">{this.state.idNumber}</label>
                                </div>
                                <div className="form-group">
                                    <input type="password" className="form-control" id="password" placeholder="Password..." required onChange={(e) => { this.updateField('password', e.target.value) }} value={this.state.password} />
                                </div>
                                <div className="form-group">
                                    <input className="form-control" id="name" placeholder="Name..." required onChange={(e) => { this.updateField('name', e.target.value) }} value={this.state.name} />
                                </div>
                                <div className="form-group">
                                    <input className="form-control" id="surname" placeholder="Surname..." required onChange={(e) => { this.updateField('surname', e.target.value) }} value={this.state.surname} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 offset-md-6">
                            <button className="btn btn-primary" type="button" onClick={(e) => { this.submit(e) }}>Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}