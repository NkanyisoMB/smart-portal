
handleValidation()
{
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;
    
    if (!fields['userName']) {
        formIsValid = false;
        errors['userName'] = 'Cannot be empty';
    }

    this.setState({ errors: errors });
    return formIsValid;
}

contactSubmit(e)
{
    e.preventDefault();

    if (this.handleValidation()) {
        alert("Form Submited");
    } else {
        alert("Form has errors");
    }
}

handleChange(field, e)
{
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({ fields });
}