import React, { Component } from 'react';
import logo from './images/logo.png';
import './App.css';
import CreateAccount from './components/Account/CreateAccount';
import NavBar from './components/nav-bar';

class App extends Component {
	render() {
		return (
			<div>
				<NavBar />
				<CreateAccount />
			</div>
		)
	}
}

export default App;