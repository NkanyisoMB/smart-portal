const http = require("http");
const server = http.createServer();

const reactPort = 3000; // put your web app port here
const apiPort = 5001; // put your ASP.Net Core web server port here
const reverseProxyPort = 7000; // use this port to test your app

server.listen(reverseProxyPort);

const host = "localhost";

server.on("request", (req, res) =>
{
	let port = reactPort;

	if(req.url.includes("/api"))
	{
	    port = apiPort;
	}
	console.log(req.method, host, req.url, port);
	
	try
	{
		const connector = http.request({
			    host: host,
			    path: req.url,
			    port: port,
			    method: req.method,
			    headers: req.headers
			  }, (resp) => {
			    res.writeHeader(resp.statusCode, resp.headers);
			    resp.pipe(res);
			  });

		req.pipe(connector);
	}
	catch(ex)
	{
	   console.error(ex);
	}
});
