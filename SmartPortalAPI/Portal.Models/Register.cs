﻿namespace Portal.Models
{
    public class Register
    {
        public string idNumber { get; set; }
        public int studentNumber { get; set; }
        public int staffId { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public int grade { get; set; }
        public string extension { get; set; }
        public string role { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
