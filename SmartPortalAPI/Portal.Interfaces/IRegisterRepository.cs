﻿using Portal.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Portal.Interfaces
{
    public interface IRegisterRepository
    {
        string CreateUserAccount(Register userDetails);
        List<Register> GetAllUsers();
        Register GetUser(string idNumber);
        string UpdateUser(Register userDetails);
        string DeleteUser(string idNumber);
        List<Register> SearchUsers(string search);
    }
}
