﻿using Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Interfaces
{
    public interface IRegisterService
    {
        List<Register> GetUsersAccounts(string userRole);
        Register GetUserAccount(string userRole, string idNumber);
        string CreateUserAccount(string userRole, Register userDetails);
        string UpdateUserAccount(string userRole, string idNumber, Register userDetails);
        string DeleteUserAccount(string userRole, string idNumber);
        List<Register> SearchUserAccount(string userRole, string search);
    }
}
