﻿using Microsoft.AspNetCore.Http;
using Portal.Interfaces;
using Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Unit.Tests.MockRepositories
{
    public class MockRegisterRepository : IRegisterRepository
    {
        private List<Register> GetUsers()
        {
            var userList = new List<Register>
            {
                new Register
                {
                    idNumber = "9404175522082",
                    studentNumber = 21345757,
                    staffId = 0,
                    name = "Nkanyiso",
                    surname = "Mbhense",
                    grade = 12,
                    extension = "B",
                    role = "Student",
                    username = "21345757",
                    password = "Passw0rd"
                },

                new Register
                {
                    idNumber = "9004175521082",
                    studentNumber = 0,
                    staffId = 5757,
                    name = "Sabelo",
                    surname = "Shange",
                    grade = 0,
                    extension = "",
                    role = "Teacher",
                    username = "5757",
                    password = "Passw0rd"
                },

                new Register
                {
                    idNumber = "9104175521082",
                    studentNumber = 0,
                    staffId = 2323,
                    name = "Apricot",
                    surname = "Mkhize",
                    grade = 0,
                    extension = "",
                    role = "Admin",
                    username = "2323",
                    password = "Passw0rd"
                }
            };

            return userList;
        }

        public string CreateUserAccount(Register userDetails)
        {
            return HttpStatusCode.Created.ToString();
        }

        public List<Register> SearchUsers(string search)
        {
            return GetUsers().Where(x=>x.idNumber.Contains(search)).ToList();
        }

        public List<Register> GetAllUsers()
        {
            return GetUsers();
        }

        public Register GetUser(string idNumber)
        {
            return GetUsers().First(x => x.idNumber.Equals(idNumber));
        }

        public string UpdateUser(Register userDetails)
        {
            if (GetUsers().Any(x => x.idNumber.Equals(userDetails.idNumber)))
            {
                return "Updated";
            }
            else
            {
                return "Not Found";
            }
        }

        public string DeleteUser(string idNumber)
        {
            if (GetUsers().Any(x => x.idNumber.Equals(idNumber)))
            {
                return "Deleted";
            }
            else
            {
                return "Not Found";
            }
        }
    }
}
