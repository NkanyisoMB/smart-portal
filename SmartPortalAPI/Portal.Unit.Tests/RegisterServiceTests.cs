﻿using System.Collections.Generic;
using NUnit.Framework;
using Portal.Interfaces;
using Portal.Services;
using Portal.Unit.Tests.MockRepositories;
using Portal.Models;
using System.Linq;
using System.Net;

namespace Portal.Unit.Tests
{
    [TestFixture]
    public class RegisterServiceTests
    {
        private IRegisterService _service;
        private IRegisterRepository _repository;
        private readonly string idNumber = "9404175522082";

        [SetUp]
        public void Initialize()
        {
            _repository = new MockRegisterRepository();
            _service = new RegisterService(_repository);
        }

        private Register GetUserDetails(string userRole)
        {
            var user = new Register
            {
                idNumber = "9404175522082",
                studentNumber = 21345755,
                staffId = 0,
                name = "Mango",
                surname = "Mbhense",
                grade = 12,
                extension = "B",
                role = "Student",
                username = "21345755",
                password = "Passw0rd"
            };

            if (userRole.Equals("Admin"))
            {
                user.role = "Teacher";
                user.studentNumber = 0;
                user.staffId = 5551;
                user.grade = 0;
                user.extension = "T";
            }

            return user;
        }

        [Test]
        public void Given_UserWithAdministratorRole_When_ViewingRegisteredUsers_Should_Return_ListOfAllRegisteredUsers()
        {
            string userRole = "Admin";
            var expectedResultCount = 3;

            var calculatedResults = _service.GetUsersAccounts(userRole);

            Assert.AreEqual(expectedResultCount, calculatedResults.Count);
            Assert.IsInstanceOf<List<Register>>(calculatedResults);
        }

        [Test]
        public void Given_UserWithTeacherRole_When_ViewingRegisteredUsers_Should_Return_ListOfRegisteredStudents()
        {
            string userRole = "Teacher";
            var expectedResultCount = 1;

            var calculatedResults = _service.GetUsersAccounts(userRole);

            Assert.AreEqual(expectedResultCount, calculatedResults.Count);
            Assert.IsInstanceOf<List<Register>>(calculatedResults);
            Assert.IsEmpty(calculatedResults.Where(x => x.role.Equals("Admin") || x.Equals("Teacher")));
        }

        [Test]
        public void Given_UserWithStudentRole_When_ViewingRegisteredUsers_Should_Return_EmptyList()
        {
            string userRole = "Student";
            var expectedResultCount = 0;

            var calculatedResults = _service.GetUsersAccounts(userRole);

            Assert.AreEqual(expectedResultCount, calculatedResults.Count);
            Assert.IsInstanceOf<List<Register>>(calculatedResults);
            Assert.IsEmpty(calculatedResults);
        }

        [Test]
        public void Given_AdminRole_And_UserIDNumber_When_SearchingUser_Should_Return_SingleUser()
        {
            string userRole = "Admin";
            string idNumber = "9004175521082";
            var expectedResultRole = "Teacher";

            var calculatedResults = _service.GetUserAccount(userRole, idNumber);

            Assert.AreEqual(expectedResultRole, calculatedResults.role);
            Assert.AreEqual(idNumber, calculatedResults.idNumber);
            Assert.IsInstanceOf<Register>(calculatedResults);
            Assert.IsNotNull(calculatedResults);
        }

        [Test]
        public void Given_TeacherRole_And_UserIDNumber_When_SearchingUser_Should_Return_SingleUser()
        {
            string userRole = "Teacher";

            var expectedResultRole = "Student";

            var calculatedResults = _service.GetUserAccount(userRole, idNumber);

            Assert.AreEqual(expectedResultRole, calculatedResults.role);
            Assert.AreEqual(idNumber, calculatedResults.idNumber);
            Assert.IsInstanceOf<Register>(calculatedResults);
            Assert.IsNotNull(calculatedResults);
        }

        [Test]
        public void Given_UserWithAdminRole_And_UserDetails_When_CreatingUserAccount_Should_Return_CreatedStatusCode()
        {
            string userRole = "Admin";

            var expectedResult = HttpStatusCode.Created.ToString();

            var calculatedResults = _service.CreateUserAccount(userRole, GetUserDetails(userRole));

            Assert.AreEqual(expectedResult, calculatedResults);
        }

        [Test]
        public void Given_UserWithTeacherRole_And_UserDetails_When_CreatingUserAccount_Should_Return_CreatedStatusCode()
        {
            string userRole = "Teacher";

            var expectedResult = HttpStatusCode.Created.ToString();

            var calculatedResults = _service.CreateUserAccount(userRole, GetUserDetails(userRole));

            Assert.AreEqual(expectedResult, calculatedResults);
        }

        [Test]
        public void Given_UserWithAdminRole_And_UserDetails_When_UpdatingUserAccount_Should_Return_Updated()
        {
            string userRole = "Admin";
            var expectedResult = "Updated";

            var calculatedResults = _service.UpdateUserAccount(userRole, idNumber, GetUserDetails(userRole));

            Assert.AreEqual(expectedResult, calculatedResults);
        }

        [Test]
        public void Given_UserWithTeacherRole_And_UserDetails_When_UpdatingUserAccount_Should_Return_Updated()
        {
            string userRole = "Teacher";
            var expectedResult = "Updated";

            var calculatedResults = _service.UpdateUserAccount(userRole, idNumber, GetUserDetails(userRole));

            Assert.AreEqual(expectedResult, calculatedResults);
        }

        [Test]
        public void Given_UserWithStudentRole_And_UserDetails_When_UpdatingUserAccount_Should_Return_Updated()
        {
            string userRole = "Student";
            var expectedResult = "Updated";

            var calculatedResults = _service.UpdateUserAccount(userRole, idNumber, GetUserDetails(userRole));

            Assert.AreEqual(expectedResult, calculatedResults);
        }

        [Test]
        public void Given_UserWithAdminRole_And_UserIdNumber_When_DeletingUserAccount_Should_Return_Deleted()
        {
            string userRole = "Admin";
            var expectedResult = "Deleted";

            var calculatedResults = _service.DeleteUserAccount(userRole, idNumber);

            Assert.AreEqual(expectedResult, calculatedResults);
        }

        [Test]
        public void Given_UserWithTeacherRole_And_UserIdNumber_When_DeletingUserAccount_Should_Return_Deleted()
        {
            string userRole = "Teacher";
            var expectedResult = "Deleted";

            var calculatedResults = _service.DeleteUserAccount(userRole, idNumber);

            Assert.AreEqual(expectedResult, calculatedResults);
        }
    }
}