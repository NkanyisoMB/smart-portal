﻿using Dapper;
using Portal.Interfaces;
using Portal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;

namespace Portal.Repositories
{
    public class RegisterRepository : IRegisterRepository
    {
        private readonly string _connection = "Data Source=SYAM;Initial Catalog=SmartPortal;Integrated Security=True";

        public string CreateUserAccount(Register userDetails)
        {
            try
            {
                using (var connection = new SqlConnection(_connection))
                {
                    connection.Execute("spInsertUser", userDetails, commandType: CommandType.StoredProcedure);
                }

                return HttpStatusCode.Created.ToString();
            }
            catch (Exception e)
            {
                return HttpStatusCode.BadRequest.ToString();
            }
        }

        public List<Register> SearchUsers(string search)
        {
            try
            {
                using (var connection = new SqlConnection(_connection))
                {
                    var parameter = new
                    {
                        search = search
                    };
                    return connection.Query<Register>("spSearchUser", parameter, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch
            {
                return new List<Register>();
            }
        }

        public List<Register> GetAllUsers()
        {
            try
            {
                using (var connection = new SqlConnection(_connection))
                {
                    return connection.Query<Register>("spGetUsers", commandType:CommandType.StoredProcedure).ToList();
                }
            }
            catch
            {
                return new List<Register>();
            }
        }

        public Register GetUser(string id)
        {
            try
            {
                using (var connection = new SqlConnection(_connection))
                {
                    var parameter = new
                    {
                        idNumber = id
                    };
                    var user =connection.Query<Register>("spGetUser", parameter, commandType: CommandType.StoredProcedure).First();
                    return user;
                }
            }
            catch(SqlException ex)
            {
                return new Register();
            }
        }

        public string UpdateUser(Register userDetails)
        {
            try
            {
                using (var connection = new SqlConnection(_connection))
                {
                    connection.Execute("spUpdateUser", userDetails, commandType: CommandType.StoredProcedure);

                    return "Updated";
                }
            }
            catch (SqlException ex)
            {
                return "404";
            }
        }

        public string DeleteUser(string id)
        {
            try
            {
                using (var connection = new SqlConnection(_connection))
                {
                    var parameter = new
                    {
                        idNumber = id
                    };

                    connection.Execute("spDeleteUser", parameter, commandType: CommandType.StoredProcedure);

                    return "Deleted";
                }
            }
            catch (SqlException ex)
            {
                return "404";
            }
    }
    }
}
