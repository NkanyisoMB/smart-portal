﻿using Portal.Interfaces;
using Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Portal.Services
{
    public class RegisterService : IRegisterService
    {
        private readonly IRegisterRepository _repository;

        public RegisterService(IRegisterRepository repository)
        {
            _repository = repository;
        }

        public string CreateUserAccount(string userRole, Register userDetails)
        {
            if (!userDetails.Equals(null))
            {
                return CreateUser(userRole, userDetails);
            }
            else
            {
                return HttpStatusCode.Forbidden.ToString();
            }
        }

        public List<Register> SearchUserAccount(string userRole, string search)
        {
            return SearchUser(userRole, search);
        }

        private List<Register> SearchUser(string userRole, string search)
        {
            if (IsAdmin(userRole))
            {
                return _repository.SearchUsers(search);
            }
            else if (IsTeacher(userRole))
            {
                return _repository.SearchUsers(search).Where(x => x.role.Equals("Student")).ToList();
            }
            else
            {
                return new List<Register>();
            }
        }

        public List<Register> GetUsersAccounts(string userRole)
        {
            return GetUsers(userRole);
        }

        public Register GetUserAccount(string userRole, string idNumber)
        {
            return GetUser(userRole, idNumber);
        }

        public string UpdateUserAccount(string userRole, string idNumber, Register userDetails)
        {
            return UpdateUser(userRole, idNumber, userDetails);
        }

        public string DeleteUserAccount(string userRole, string idNumber)
        {
            return DeleteUser(userRole, idNumber);
        }

        private string DeleteUser(string userRole, string idNumber)
        {
            if (IsAdmin(userRole))
            {
                return _repository.DeleteUser(idNumber);
            }
            else if (IsTeacher(userRole))
            {
                return _repository.DeleteUser(idNumber);
            }
            else
            {
                return HttpStatusCode.Unauthorized.ToString();
            }
        }

        private string UpdateUser(string userRole, string idNumber, Register userDetails)
        {
            if (IsAdmin(userRole))
            {
                return _repository.UpdateUser(userDetails);
            }
            else if (IsTeacher(userRole) && IsStudent(userDetails.role) || IsTeacher(userDetails.role))
            {
                return _repository.UpdateUser(userDetails);
            }
            else if (IsStudent(userRole) && IsStudent(userDetails.role))
            {
                return _repository.UpdateUser(userDetails);
            }
            else
            {
                return HttpStatusCode.Unauthorized.ToString();
            }
        }

        private Register GetUser(string userRole, string idNumber)
        {
            if (IsAdmin(userRole))
            {
                return _repository.GetUser(idNumber);
            }
            else if (IsTeacher(userRole))
            {
                var user = _repository.GetUser(idNumber);

                if (!IsAdmin(user.role) && !IsTeacher(user.role))
                {
                    return user;
                }
                else
                {
                    return new Register();
                }
            }
            else
            {
                return new Register();
            }
        }

        private string CreateUser(string userRole, Register userDetails)
        {
            if (IsAdmin(userRole))
            {
                return _repository.CreateUserAccount(userDetails);
            }
            else if (IsTeacher(userRole) && IsStudent(userDetails.role))
            {
                return _repository.CreateUserAccount(userDetails);
            }
            else
            {
                return HttpStatusCode.Unauthorized.ToString();
            }
        }

        private List<Register> GetUsers(string userRole)
        {
            if (IsAdmin(userRole))
            {
                return _repository.GetAllUsers();
            }
            else if (IsTeacher(userRole))
            {
                return _repository.GetAllUsers().Where(x => x.role.Equals("Student")).ToList();
            }
            else
            {
                return new List<Register>();
            }
        }

        private Boolean IsAdmin(string userRole)
        {
            return userRole.Equals("Admin");
        }

        private Boolean IsTeacher(string userRole)
        {
            return userRole.Equals("Teacher");
        }

        private Boolean IsStudent(string userRole)
        {
            return userRole.Equals("Student");
        }
    }
}
