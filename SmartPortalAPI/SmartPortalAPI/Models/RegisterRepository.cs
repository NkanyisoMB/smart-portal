﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;

namespace SmartPortalAPI.Models
{
    public class RegisterRepository
    {
        string sql = "SELECT * FROM User";

        public List<Register> GetAllUsers()
        {
            using (var connection = new SqlConnection(""))
            {
                var users = connection.Query<Register>(sql).ToList();
                return users;
            }
        }
    }
}
