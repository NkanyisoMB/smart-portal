﻿using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Portal.Interfaces;
using Portal.Models;
using Portal.Repositories;
using Portal.Services;

namespace Portal.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        private readonly IRegisterService registerService;

        public RegisterController()
        {
            registerService = new RegisterService(new RegisterRepository());
        }

        // GET: api/Register/search
        [HttpGet]
        [Route("users/{role}/{search}")]
        public IEnumerable<Register> SearchUsers(string role, string search)
        {
            return registerService.SearchUserAccount(role, search);
        }

        // GET: api/Register
        [HttpGet]
        [Route("users/{userRole}")]
        public IEnumerable<Register> GetUsers(string userRole)
        {
            return registerService.GetUsersAccounts(userRole);
        }

        // GET: api/Register/5
        [HttpGet("{id}")]
        [Route("{role}/{id}")]
        public Register Get(string role, string id)
        {
            return registerService.GetUserAccount(role, id);
        }

        // POST: api/Register
        [HttpPost]
        [Route("{userRole}")]
        public HttpStatusCode Post(string userRole, [FromBody] Register user)
        {
            string response = registerService.CreateUserAccount(userRole, user);
            return response.Equals("Created")? HttpStatusCode.Created : HttpStatusCode.BadRequest;
        }

        // PUT: api/Register/5
        [HttpPut("{role}/{id}")]
        public HttpStatusCode Put(string role, string id, [FromBody] Register user)
        {
            string response = null;
            response = registerService.UpdateUserAccount(role, id, user);
            return response.Equals("Updated") ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{role}/{id}")]
        public HttpStatusCode Delete(string role, string id)
        {
            string response = registerService.DeleteUserAccount(role, id);
            return response.Equals("Deleted") ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
        }
    }
}
